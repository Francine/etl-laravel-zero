## ETL Laravel Zero

ETL Laravel Zero funciona em linha de comando onde extrai dados de um arquivo .txt, transforma e salva na base de dados.

## Requisitos

PHP >= 7.1.3

COMPOSER

MYSQL 5.7

## Como executar o projeto

**Instalar dependências:**

`composer install`

**Migrar o banco:**

`php etl-laravel-zero migrate`


### Para extrair os dados

`php etl-laravel-zero extract`


### Para gerar um relatório

O arquivo gerado fica localizado dentro da pasta storage do projeto

`php etl-laravel-zero generate:report`