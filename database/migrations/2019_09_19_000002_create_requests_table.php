<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'requests';

    /**
     * Run the migrations.
     * @table requests
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('method', 45)->nullable();
            $table->string('uri', 45)->nullable();
            $table->string('url', 45)->nullable();
            $table->integer('size')->nullable();
            $table->string('querystring', 45)->nullable();
            $table->integer('headers_id')->unsigned();

            $table->foreign('headers_id')
                ->references('id')->on('headers'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
