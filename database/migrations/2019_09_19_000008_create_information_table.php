<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'information';

    /**
     * Run the migrations.
     * @table information
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('requests_id')->unsigned();
            $table->integer('responses_id')->unsigned();
            $table->integer('routes_id')->unsigned();
            $table->integer('services_id')->unsigned();
            $table->integer('latencies_id')->unsigned();
            $table->string('upstream_uri', 45);
            $table->string('consumer_id', 45);
            $table->string('client_ip', 45);
            $table->dateTime('started_at')->nullable();

            $table->foreign('requests_id')
                ->references('id')->on('requests')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('responses_id')
                ->references('id')->on('responses')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('routes_id')
                ->references('id')->on('routes')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('services_id')
                ->references('id')->on('services')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('latencies_id')
                ->references('id')->on('latencies')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
