<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'routes';

    /**
     * Run the migrations.
     * @table routes
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('hosts', 45)->nullable();
            $table->string('methods', 45)->nullable();
            $table->string('paths', 45)->nullable();
            $table->string('preserve_host', 45)->nullable();
            $table->string('protocols', 45)->nullable();
            $table->integer('regex_priority')->nullable();
            $table->string('strip_path', 45)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->integer('services_id')->unsigned();

            $table->foreign('services_id')
                ->references('id')->on('services'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
