<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'services';

    /**
     * Run the migrations.
     * @table services
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('id_key')->nullable();
            $table->integer('connect_timeout')->nullable();
            $table->string('host', 45)->nullable();
            $table->string('name', 45)->nullable();
            $table->string('path', 45)->nullable();
            $table->integer('port')->nullable();
            $table->string('protocol', 45)->nullable();
            $table->integer('read_timeout')->nullable();
            $table->integer('retries')->nullable();
            $table->integer('write_timeout')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
