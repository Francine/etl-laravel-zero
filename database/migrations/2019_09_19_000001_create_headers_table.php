<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'headers';

    /**
     * Run the migrations.
     * @table headers
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('accept', 45)->nullable();
            $table->string('host', 45)->nullable();
            $table->string('user_agent', 45)->nullable();
            $table->string('content_length', 45)->nullable();
            $table->string('access_control_allow_credentials', 45)->nullable();
            $table->string('content_type', 45)->nullable();
            $table->string('server', 45)->nullable();
            $table->string('connection', 45)->nullable();
            $table->string('via', 45)->nullable();
            $table->string('access_control_allow_origin', 45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
