<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use App\Repositories\GenerateReportRepository;

class GenerateReport extends Command
{
    private $report;

    public function __construct() {
        $this->report = new GenerateReportRepository();
        parent::__construct();
    }

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'generate:report';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Generate report about information';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->report->handle();
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
