<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use App\Repositories\InformationRepository;

class ExtractInformation extends Command
{
    private $solicitation;

    public function __construct() {
        $this->solicitation = new InformationRepository();
        parent::__construct();
    }
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'extract';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Extract and save an information';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->solicitation->handle();
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
