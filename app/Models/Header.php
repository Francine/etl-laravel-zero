<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Header extends Model{

    protected $fillable = ['accept', 'host', 'user_agent'];

    public $timestamps = false;

    public function request() {
        return $this->hasOne('App\Models\Request', 'headers_id');
    }

    public function response() {
        return $this->hasOne('App\Models\Response', 'headers_id');
    }
}