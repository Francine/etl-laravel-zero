<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = ['hosts', 'methods', 'paths', 'preserve_host', 'protocols', 'regex_priority', 'strip_path'];

    public $timestamps = false;

    public function setMethods($methods) {
        $this->methods = implode(', ',$methods);
    }

    public function setPaths($paths) {
        $this->paths = implode(', ',$paths);
    }

    public function setProtocols($protocols) {
        $this->protocols = implode(', ',$protocols);
    }
}
