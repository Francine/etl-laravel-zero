<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Latencies extends Model
{
    protected $fillable = ['proxy', 'kong', 'request'];

    public $timestamps = false;

    public function information() {
        return $this->belongsTo('App\Models\Information', 'latencies_id');
    }
}
