<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Information extends Model{

    protected $fillable = ['upstream_uri', 'consumer_id', 'client_ip', 'consumer_id'];

    public $timestamps = false;

    public function request() {
        return $this->hasOne('App\Models\Request');
    }

    public function information() {
        return $this->hasOne('App\Models\Service', 'services_id');
    }

    public function latencies() {
        return $this->hasOne('App\Models\Latencies', 'latencies_id');
    }
}