<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model{

    protected $fillable = ['method', 'uri', 'url', 'size', 'queryString'];

    public $timestamps = false;

    public function header() {
        return $this->belongsTo('App\Models\Header', 'headers_id');
    }

    public function information() {
        return $this->belongsTo('App\Models\Information');
    }

    public function setQueryString($queryString) {
        $this->queryString =  implode(', ',$queryString);
    }
}