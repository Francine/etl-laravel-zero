<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['id_key', 'connect_timeout', 'host', 'name', 'path', 'port', 'protocol', 'read_timeout', 'retries', 'write_timeout'];

    public $timestamps = false;

    public function information() {
        return $this->belongsTo('App\Models\Information', 'services_id');
    }
}
