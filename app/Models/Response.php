<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model{

    protected $fillable = ['status', 'size'];

    public $timestamps = false;

    public function header() {
        return $this->belongsTo('App\Models\Header', 'headers_id');
    }
}