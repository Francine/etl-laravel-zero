<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Log;
use App\Models\Information;
use App\Models\Latencies;
use App\Models\Response;
use App\Models\Request;
use App\Models\Service;
use App\Models\Header;
use App\Models\Route;

class InformationRepository implements BaseRepository
{
   public function handle() {
      $this->extract();
   }

   public function extract() {
      $this->transform();

      foreach($this->file as $key => $data) {
         $data = json_decode($data);

         $this->createRecords($data);
         $this->revertModels();
         
         echo ('Record '. $key .' created succesfully');
         echo "\n";
      }

      $this->revertModels();
   }

   public function transform() {
      $this->file = $this->getFile('./logs.txt');

      $this->file = str_replace("user-agent","user_agent",$this->file);   
      $this->file = str_replace("access-control-allow-origin","access_control_allow_origin",$this->file);   
      $this->file = str_replace("Content-Length","content_length",$this->file);   
      $this->file = str_replace("access-control-allow-credentials","access_control_allow_credentials",$this->file);   
      $this->file = str_replace("Content-Type","content_type",$this->file);   
   }

   private function createRecords($data) {
      $this->createHeaderResponse($data->response->headers);
      $this->createHeader($data->request->headers);
      $this->createRequest($data->request);
      $this->createResponse($data->response);
      $this->createService($data->service);
      $this->createRoute($data->route);
      $this->createLatencies($data->latencies);
      $this->createInformation($data);
   }

   private function createHeader($header) {
      $this->header = new Header();
      $this->header->accept = $header->accept;
      $this->header->host = $header->host;
      $this->header->user_agent = $header->user_agent;
      $this->header->save();
   }

   private function createHeaderResponse($headerResponse) {
      $this->headerResponse = new Header();
      $this->headerResponse->content_length = $headerResponse->content_length;
      $this->headerResponse->via = $headerResponse->via;
      $this->headerResponse->connection = $headerResponse->Connection;
      $this->headerResponse->access_control_allow_credentials = $this->headerResponse->access_control_allow_credentials;
      $this->headerResponse->content_type = $this->headerResponse->content_type;
      $this->headerResponse->server = $this->headerResponse->server;
      $this->headerResponse->access_control_allow_origin = $this->headerResponse->access_control_allow_origin;
      $this->headerResponse->save();
   }

   private function createRequest($request) {
      $this->request = new Request();
      $this->request->method = $request->method;
      $this->request->uri = $request->uri;
      $this->request->url = $request->url;
      $this->request->size = $request->size;
      $this->request->setQueryString($request->querystring);
      $this->request->headers_id = $this->header->id;
      $this->request->save();
   }

   private function createResponse($response) {
      $this->response = new Response();
      $this->response->status = $response->status;
      $this->response->size = $response->size;
      $this->response->headers_id = $this->headerResponse->id;
      $this->response->save();
   }

   private function createService($service) {

      $this->service = $this->verifyService($service->id);
      
      if(empty($this->service)) {
         
         $this->service = new Service();
         $this->service->connect_timeout = $service->connect_timeout;
         $this->service->id_key = $service->id;
         $this->service->host = $service->host;
         $this->service->name = $service->name;
         $this->service->path = $service->path;
         $this->service->port = $service->port;
         $this->service->protocol = $service->protocol;
         $this->service->read_timeout = $service->read_timeout;
         $this->service->retries = $service->retries;
         $this->service->write_timeout = $service->write_timeout;
         $this->service->created_at = $this->convertDate($service->created_at);
         $this->service->updated_at = $this->convertDate($service->updated_at);
         $this->service->save();  
      }
   }

   private function verifyService($key) {
      $service = Service::where('id_key', $key)->get()->first();;
      return $service;
   }

   private function createRoute($route) {
      $this->route = new Route();
      $this->route->hosts = $route->hosts;
      $this->route->preserve_host = $route->preserve_host;
      $this->route->regex_priority = $route->regex_priority;
      $this->route->strip_path = $route->strip_path;
      $this->route->setMethods($route->methods);
      $this->route->setPaths($route->paths);
      $this->route->setProtocols($route->protocols);
      $this->route->services_id = $this->service->id;
      $this->route->created_at = $this->convertDate($route->created_at);
      $this->route->updated_at = $this->convertDate($route->updated_at);
      $this->route->save();
   }

   private function createLatencies($latencies) {
      $this->latencies = new Latencies();
      $this->latencies->proxy = $latencies->proxy;
      $this->latencies->kong = $latencies->kong;
      $this->latencies->request = $latencies->request;
      $this->latencies->save();  
   }

   private function createInformation($data) {
      $this->information = new Information();
      $this->information->requests_id = $this->request->id;
      $this->information->responses_id = $this->response->id;
      $this->information->routes_id = $this->route->id;
      $this->information->services_id = $this->service->id;
      $this->information->latencies_id = $this->latencies->id;
      $this->information->upstream_uri = $data->upstream_uri;
      $this->information->client_ip = $data->client_ip;
      $this->information->consumer_id = $data->authenticated_entity->consumer_id->uuid;
      $this->information->started_at = $this->convertDate($data->started_at);
      $this->information->save(); 
   }

   private function revertModels() {
      $this->headerResponse = null;
      $this->header = null;
      $this->request = null;
      $this->response = null;
      $this->service = null;
      $this->route = null;
      $this->latencies = null;
      $this->information = null;
   }

   private function getFile(string $path) {
      $this->file = file($path);
      return $this->file;
   }

   private function convertDate($date) {
      $date = date("Y-m-d H:i:s", $date);
      return $date;
   }
}