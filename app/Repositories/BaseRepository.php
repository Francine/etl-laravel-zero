<?php

namespace App\Repositories;

interface BaseRepository
{
   public function extract();

   public function transform();
}