<?php

namespace App\Repositories;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use App\Exports\ReportExport;
use App\Models\Information;
use App\Models\Latencies;
use App\Models\Service;

class GenerateReportRepository
{
   private $consumer;
   private $service;
   private $request;
   private $proxy;
   private $kong;

   public function handle() {
      $this->addExtraColumns();
      $this->generateReport();
   }

   private function generateReport() {
      Excel::store(new ReportExport($this->consumer, $this->service, $this->request, $this->proxy, $this->kong), 'reports.csv');
   }

   private function addExtraColumns() {
      $this->countByConsumer();
      $this->countByServiceKey();
      $this->calculateAverage();
   }

   private function countByServiceKey() {
      $this->service = Service::count(DB::raw('DISTINCT id_key'));
   }

   private function countByConsumer() {
      $this->consumer = Information::count(DB::raw('DISTINCT consumer_id'));
   }

   private function calculateAverage() {
      $ids = Service::select('id_key')->distinct('id_key')->get()->toArray();
      foreach ($ids as $id) {
        $this->avgProxy($id);
        $this->avgRequest($id);
        $this->avgKong($id);
      }
   }

   private function avgProxy($id){
      $this->proxy[$id['id_key']] = 'Tempo médio de Proxy no service com id - '.Latencies::with(['information' => function ($query) {
         $query->with(['service' => function ($q) {
            $q->where('id_key', $id['id_key']);
         }]);
      }])->avg('proxy');
   }

   private function avgRequest($id){
      $this->request[$id['id_key']] = 'Tempo médio de Request no service com id - '.Latencies::with(['information' => function ($query) {
         $query->with(['service' => function ($q) {
            $q->where('id_key', $id['id_key']);
         }]);
      }])->avg('request');
   }

   private function avgKong($id){
      $this->kong[$id['id_key']] = 'Tempo médio de Kong no service com id - '.Latencies::with(['information' => function ($query) {
         $query->with(['service' => function ($q) {
            $q->where('id_key', $id['id_key']);
         }]);
      }])->avg('kong');
   }
}