<?php

namespace App\Exports;

use App\Models\Information;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;

class ReportExport implements FromCollection
{
  private $information;

  public function __construct($consumer, $service, $request, $proxy, $kong) {
    $this->consumer = ['Requisições por consumidor - '.$consumer];
    $this->service = ['Requisições por serviço - '.$service];
    $this->request = $request;
    $this->proxy = $proxy;
    $this->kong = $kong;
  }
  public function collection()
  {
    $this->information = Information::all();
    $this->addExtraColumns();
    return $this->information;
  }

  public function addExtraColumns() {
    $this->information->push($this->consumer);
    $this->information->push($this->service);
    $this->information->push($this->request);
    $this->information->push($this->proxy);
    $this->information->push($this->kong);
  }
}
